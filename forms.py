__author__ = 'leon'
from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=15, required=True)
    password = forms.CharField(max_length=15, required=True)
