from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import auth
from forms import LoginForm


def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        print request.POST
        if form.is_valid():
            username = form.cleaned_data['username']
            print form.cleaned_data
            password = form.cleaned_data['password']
            user = auth.authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    auth.login(request, user)
                    return HttpResponseRedirect('/')
            else:
                return HttpResponseRedirect('/')
            return HttpResponseRedirect('/')
    else:
        form = LoginForm()
    return render(request, 'base.html', {
        'form': form,
    })


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')